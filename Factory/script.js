var test = angular.module('testApp', ['ngRoute']);

test.directive('headerTitle', function () {
    return {
        restrict: 'E',

        templateUrl: 'header.html',
    }
});

test.factory('factoryOrang', function ($http) {
    var factoryOrang = {};
    factoryOrang.getManusia = function () {
        return $http.get('data.php');
    }
    return factoryOrang;
});
test.directive('partTitle', function () {
    return {
        restrict: 'A',

        templateUrl: '../Part-Template/part.html',
        controller: function ($scope, factoryOrang) {
            factoryOrang.getManusia().then(function (hasil) {
                $scope.data = hasil.data;
            })
        }
    }
});
// Route
test.config(function ($routeProvider) {
    $routeProvider
        .when('/tambah-info', {
            templateUrl: 'index2.html',
            controller: 'add'
        })
        .when('/kontak', {
            templateUrl: 'kontak.html'

        })
        .otherwise({
            redirectTo: '/'
        });
});

test.controller('add', function ($scope) {
    $scope.tambahData = function () {
        $scope.data.push({
            nama: $scope.databaru.nama,
            kota: $scope.databaru.kota
        });
    }
})