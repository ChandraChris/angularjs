var test = angular.module('testApp', ['ngRoute']);

test.directive('headerTitle', function () {
    return {
        restrict: 'E',

        templateUrl: '../Part-Template/header.html',
    }
});

test.directive('partTitle', function () {
    return {
        restrict: 'A',

        templateUrl: '../Part-Template/part.html',
        controller: function ($scope) {
            $scope.data = [{
                    nama: "Souma",
                    kota: "Tokyo"
                },
                {
                    nama: "Nakiri",
                    kota: "Shibuya"
                },
                {
                    nama: "Joichiro",
                    kota: "Jakarta"
                }
            ];
        }
    }
});
// Route
test.config(function ($routeProvider) {
    $routeProvider
        .when('/tambah-info', {
            templateUrl: 'index2.html',
            controller: 'add'
        })
        .otherwise({
            redirectTo: '/'
        });
});

test.controller('add', function ($scope) {
    $scope.tambahData = function () {
        $scope.data.push({
            nama: $scope.databaru.nama,
            kota: $scope.databaru.kota
        });
    }
})