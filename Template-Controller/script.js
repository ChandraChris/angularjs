var test = angular.module('testApp', []);

test.directive('headerTitle', function () {
    return {
        restrict: 'E',

        templateUrl: '../Part-Template/header.html',
    }
});

test.directive('partTitle', function () {
    return {
        restrict: 'A',

        templateUrl: '../Part-Template/part.html',
        controller: function ($scope) {
            $scope.data = [{
                    nama: "Souma",
                    kota: "Tokyo"
                },
                {
                    nama: "Nakiri",
                    kota: "Shibuya"
                },
                {
                    nama: "Joichiro",
                    kota: "Jakarta"
                }
            ];
        }
    }
});