// Module Angular
var test = angular.module('test-app', []);
// Controller
test.controller('dataOrang', function ($scope) {
    $scope.data = [{
            nama: 'Souma',
            kota: 'Tokyo'
        },
        {
            nama: 'Nakiri',
            kota: 'Shibuya'
        },
        {
            nama: 'Takumi',
            kota: 'Paris'
        }
    ];
});